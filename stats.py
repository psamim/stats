#/usr/bin/env python
import subprocess
import re
from jinja2 import Template
from datetime import date
from datetime import timedelta
from datetime import datetime
from functools import reduce
import os
import gitlab
from pyclickup import ClickUp


def last_day(d, day_name):
    """Get the last date of weekday before today"""
    days_of_week = [
        'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday',
        'saturday'
    ]
    target_day = days_of_week.index(day_name.lower())
    delta_day = target_day - d.isoweekday()
    if delta_day >= 0: delta_day -= 7  # go back 7 days
    return d + timedelta(days=delta_day)


# constants
PROJECT_ID = os.environ["CI_PROJECT_ID"]
GITLAB_TOKEN = os.environ['GITLAB_TOKEN']
CLICKUP_TOKEN = os.environ['CLICKUP_TOKEN']
GIT_RPEO = os.environ['CI_PROJECT_DIR']
AFTER = os.getenv('AFTER') or last_day(date.today(), "wednesday").isoformat()
BEFORE = os.getenv('BEFORE') or date.today().isoformat()
CLICKUP_PROJECT_NAME = os.getenv('CLICKUP_PROJECT_NAME') or 'Frontend'
MAX_AUTHORS = 5

# globals
mergerequests = []
tasks = []


def get_name(author):
    name = re.search('^(.*)<', author)
    if name is not None:
        name = name.group(1)
        return name.strip()


def get_email(author):
    email = re.search('<(.*)>', author)
    if email is not None:
        email = email.group(1)
        return email.strip()


def get_gitalab_mergerequests(after, before):
    global mergerequests
    if len(mergerequests) != 0:
        return mergerequests

    gl = gitlab.Gitlab('https://gitlab.com', private_token=GITLAB_TOKEN)
    project = gl.projects.get(PROJECT_ID)
    mergerequests = project.mergerequests.list(
        state="merged", created_after=after, created_before=before)
    return mergerequests


def get_clickup_tasks():
    global tasks
    if len(tasks) != 0:
        return tasks

    clickup = ClickUp(CLICKUP_TOKEN)
    main_team = clickup.teams[0]
    main_space = main_team.spaces[0]
    projects = main_space.projects
    project = next(
        filter(lambda p: p.name == CLICKUP_PROJECT_NAME, projects), None)
    if project is not None:
        tasks = project.get_all_tasks(include_closed=True, order_by='due_date')

    return tasks


def tasks_in_period(after_date, before_date):
    tasks = get_clickup_tasks()
    after = datetime.combine(
        date.fromisoformat(after_date), datetime.min.time())
    before = datetime.combine(
        date.fromisoformat(before_date), datetime.min.time())

    def predicate(task):
        if task.date_closed is not None:
            return task.date_closed > after and task.date_closed < before
        return False

    tasks = list(filter(predicate, tasks))
    return tasks


def tasks_for_author_in_period(author):
    tasks = tasks_in_period()
    name = get_name(author)

    def predicate(task):
        task_authors = list(
            filter(lambda x: re.search(name, x.username, re.IGNORECASE),
                   task.assignees))
        return len(task_authors) > 0

    tasks = list(filter(predicate, tasks))
    return tasks


def tasks_by_tag():
    tasks = tasks_in_period()

    def by_tag(result, task):
        if (len(task.tags) == 0):
            prev_count = result.get("other", 0)
            result["other"] = prev_count + 1

        for tag in task.tags:
            prev_count = result.get(tag.name, 0)
            result[tag.name] = prev_count + 1

        return result

    result = reduce(by_tag, tasks, {})
    return result


def number_of_merge_requests(author, after, before):
    name = get_name(author)
    mergerequests = get_gitalab_mergerequests(after, before)
    mrs = list(
        filter(lambda i: re.match(name, i.author["name"], re.IGNORECASE),
               mergerequests))
    return len(mrs)


def run(command):
    cmd = ('cd %s && ' % GIT_RPEO) + command
    out = subprocess.run(["/bin/bash", "-c", cmd], capture_output=True)
    return str(out.stdout.decode())


def number_of_lines_for_author_in_period(author, after, before):
    command = (
        'git log --author="%s" --after=%s --before=%s --no-merges --pretty=tformat: --numstat'
        ' | awk -F" " \'{ added += $1; removed += $2 } END { print added,",",removed }\''
    )
    command = command % (get_email(author), after, before)
    numbers = run(command)
    matches = re.search("(\d+) , (\d+)", numbers)
    if matches is not None:
        added = int(matches.group(1))
        removed = int(matches.group(2))
        return {
            "lines": added + removed,
            "changes": "+%s,-%s" % (added, removed)
        }

    return {"lines": 0, "changes": ""}


def overall_number_of_lines_in_period(after, before):
    command = (
        'git log  --after=%s --before=%s --no-merges --pretty=tformat: --numstat'
        ' | awk -F" " \'{ added += $1; removed += $2 } END { print added,",",removed }\''
    )
    command = command % (after, before)
    numbers = run(command)
    matches = re.search("(\d+) , (\d+)", numbers)
    if matches is not None:
        added = int(matches.group(1))
        removed = int(matches.group(2))
        return {
            "lines": added + removed,
            "changes": "+%s,-%s" % (added, removed)
        }

    return {"lines": 0, "changes": ""}


def authors():
    command = 'git --no-pager shortlog -es --no-merges  | cut -f2'
    authors = run(command)
    authors = authors.splitlines()
    return list(authors)


def number_of_commits_for_author_in_period(author, after, before):
    command = 'git --no-pager shortlog -es --after=%s --before=%s --author=%s --no-merges' % (
        after, before, get_email(author))
    number = run(command)
    number = re.search('\d+', number)
    if number is not None:
        return number.group(0)


def overall_number_of_commits_for_author_with_index(index):
    command = 'git --no-pager shortlog -es --no-merges'
    number = run(command)
    number = number.splitlines()
    if index < len(number):
        number = number[index]
        number = re.search('\d+', number)
        if number is not None:
            return number.group(0)


def overall_number_of_commits_in_period(after, before):
    command = 'git rev-list --no-merges --all --count --after=%s --before=%s master' % (
        after, before)
    number = run(command)
    number = re.search("\d*", number)
    if number is not None:
        number = number.group(0)
        number = int(number)
        return number


overview = {
    "commits": overall_number_of_commits_in_period(AFTER, BEFORE),
    "mergerequests": len(get_gitalab_mergerequests(AFTER, BEFORE)),
    "loc": overall_number_of_lines_in_period(AFTER, BEFORE),
    "tasks": len(tasks_in_period(AFTER, BEFORE)),
}

# Have all authors
results = []
for idx, author in enumerate(authors()):
    results.append({
        "name":
        author,
        "number_of_commits":
        overall_number_of_commits_for_author_with_index(idx),
    })

# Sort by number of commits
results = sorted(
    results, key=lambda x: int(x["number_of_commits"]), reverse=True)

# Only top
results = results[:MAX_AUTHORS]

# Overall top authors
overall_top = results.copy()

# Number of commits this period
this_period_top = map(
    lambda x: {
        **x, "number_of_commits":
        number_of_commits_for_author_in_period(x["name"], AFTER, BEFORE)
    }, results)
this_period_top = sorted(
    this_period_top, key=lambda x: int(x["number_of_commits"]), reverse=True)

# Number of merge requests
no_mergerequests = map(
    lambda x: {
        **x, "mergerequests": number_of_merge_requests(x["name"], AFTER, BEFORE
                                                       )
    }, results)
no_mergerequests = sorted(
    no_mergerequests, key=lambda x: int(x["mergerequests"]), reverse=True)

# Number of lines
no_lines = map(
    lambda x: {
        **x,
        **number_of_lines_for_author_in_period(x["name"], AFTER, BEFORE),
    }, results)
no_lines = sorted(no_lines, key=lambda x: int(x["lines"]), reverse=True)

pwd = os.path.dirname(os.path.realpath(__file__))
with open(pwd + '/stats.html', 'r') as template:
    template = Template(template.read())
    x = template.render(
        overview=overview,
        overall_top=overall_top,
        this_period_top=this_period_top,
        lines=no_lines,
        mergerequests=no_mergerequests)
    print(x)
