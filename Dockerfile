FROM python:3
ADD requirements.txt /app/
RUN pip install -r /app/requirements.txt
RUN apt-get update && apt-get install -y --no-install-recommends \
		git \
&& rm -rf /var/lib/apt/lists/*
ADD . /app
ENV CI_PROJECT_DIR="/repo"
CMD [ "python", "/app/stats.py" ]
